﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour {
	public float moveSpeed ;
	public float turnSpeed;
	public float baseSpeed;
	public float startTimer;
	private float halfSpeed = 0.5f;
	private float hundredDivide = 0.1f;
	public bool canMove;

	private Transform tf;
	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		canMove = false;
		StartCoroutine (ReadyPlayerOne ());
		
	}
	
	// Update is called once per frame
	void Update () {
		if (canMove == true) {
			tf.position += tf.up * baseSpeed * hundredDivide;
			if (Input.GetKey (KeyCode.W)) {
				tf.position += tf.up * moveSpeed * hundredDivide;
			}
			if (Input.GetKeyDown (KeyCode.S)) {
				baseSpeed = (baseSpeed * halfSpeed);
			}
			if (Input.GetKeyUp (KeyCode.S)) {
				baseSpeed = (baseSpeed * 2);
			}
			if (Input.GetKey (KeyCode.A)) {
				tf.Rotate (Vector3.forward * turnSpeed);
			}
			if (Input.GetKey (KeyCode.D)) {
				tf.Rotate (Vector3.back * turnSpeed);
			}
		}
	}
	IEnumerator ReadyPlayerOne () {
		yield return new WaitForSeconds (startTimer);
		canMove = true;
	}
}
