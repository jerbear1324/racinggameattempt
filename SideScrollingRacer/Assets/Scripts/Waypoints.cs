﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour {
	public Transform waypointOne;
	public Transform waypointTwo;
	public Transform waypointThree;

	public Transform targetWaypoint;
	private Transform tf;
	public Vector3 movementVector;

	public float speed;

	public float waitTimer1;
	public float waitTimer2;
	public float waitTimer3;
	public float startTimer;

	public Transform testBox;
	public Vector3 targetVector;
	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		StartCoroutine (DelayWayPointOne ());
		
	}
	
	// Update is called once per frame
	void Update () {
		tf.transform.rotation = targetWaypoint.transform.rotation;
		tf.position += tf.up * speed * .01f;
		
	}
	IEnumerator DelayWayPointOne () {
		yield return new WaitForSeconds (startTimer);
		WayPointOne();
		Debug.Log ("one");
	}

	public void WayPointOne() {
		targetWaypoint = waypointOne;
		StartCoroutine (DelayWayPointTwo ());
	}

	IEnumerator DelayWayPointTwo () {
		yield return new WaitForSeconds (waitTimer1);
		WayPointTwo();
		Debug.Log ("two");
	}

	public void WayPointTwo () {
		targetWaypoint = waypointTwo;
		StartCoroutine (DelayWayPointThree ());
	}

	IEnumerator DelayWayPointThree() {
		yield return new WaitForSeconds (waitTimer2);
		WayPointThree();
		Debug.Log ("three");
	}

	public void WayPointThree () {
		targetWaypoint = waypointThree;
	}

	//IT WORKS!!! CHANGE ALL CODE ABOVE TO THIS METHOD!!!
	//I lied, it doesn't
	public void OnTriggerEnter2D(Collider2D testBox) {
		targetWaypoint = waypointOne;
	}
}
