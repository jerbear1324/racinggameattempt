﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (DiePotato ());
	}

	IEnumerator DiePotato ()
	{
		yield return new WaitForSeconds (3);
		Destroy (this.gameObject);
	}
}
