﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneBoss : MonoBehaviour {
	public void MainMenu () {
		SceneManager.LoadScene ("MainMenu", LoadSceneMode.Single);
	}
	public void OneTacOne () {
		SceneManager.LoadScene ("MainEvent", LoadSceneMode.Single);
	}
	public void OptionsMenu () {
		SceneManager.LoadScene ("Options", LoadSceneMode.Single);
	}
	public void LevelSelection () {
		SceneManager.LoadScene ("LevelSelection", LoadSceneMode.Single);
	}
	public void DragRaceAI () {
		SceneManager.LoadScene ("DragRaceAI", LoadSceneMode.Single);
	}
	public void DragRaceHuman () {
		SceneManager.LoadScene ("DragRaceHuman", LoadSceneMode.Single);
	}
	public void Quit () {
		Application.Quit ();
	}
}
