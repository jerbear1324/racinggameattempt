﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuRaceCars : MonoBehaviour {
	public Transform raceCar;
	private bool start;
	public float spawnTime;

	void Update (){
		spawnTime = Random.Range (1, 6);
	}
	void Start () {
		Debug.Log("uh");
		start = true;
		StartCoroutine (RaceCar ());
	}
	IEnumerator RaceCar ()
	{
		if (start == true) {
			Debug.Log ("Woo");
			Instantiate (raceCar, transform.position, transform.rotation);
			start = false;
			yield return new WaitForSeconds (spawnTime);
			StartCoroutine(UnoMas ());
		}
	}
	IEnumerator UnoMas ()
	{
		yield return new WaitForSeconds (1);
		start = true;
		StartCoroutine (RaceCar ());
	}
}
