﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragRaceAI : MonoBehaviour {
	public float moveSpeed ;
	public float randomSpeed;
	public float startTimer;
	private float hundredDivide = 0.1f;
	public bool canMove;

	private Transform tf;
	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		canMove = false;
		StartCoroutine (ReadyPlayerOne ());
		
	}
	
	// Update is called once per frame
	void Update () {
		if (canMove == true) {
			tf.position += tf.up * randomSpeed * hundredDivide * .55f;
			randomSpeed = Random.Range (1.0f, 3.0f);
			}
		}
	IEnumerator ReadyPlayerOne () {
		yield return new WaitForSeconds (startTimer);
		canMove = true;
	}
}
