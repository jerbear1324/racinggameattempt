﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (DiePotato ());
	}

	IEnumerator DiePotato ()
	{
		yield return new WaitForSeconds (5);
		Destroy (this.gameObject);
	}
}
