﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DRP1 : MonoBehaviour {
	public float moveSpeed ;
	public float turnSpeed;
	public float baseSpeed;
	public float startTimer;
	private float hundredDivide = 0.1f;
	public bool canMove;
	public bool canShift;
	public bool normalSpeed;
	public float shiftOne;
	public float shiftTwo;
	public float shiftThree;
	public Transform shiftText;

	private Transform tf;
	// Use this for initialization
	void Start () {
		normalSpeed = false;
		tf = GetComponent<Transform> ();
		canMove = false;
		StartCoroutine (ReadyPlayerOne ());
		canShift = false;
		StartCoroutine (ShiftOne());
		StartCoroutine (ShiftTwo());
		StartCoroutine (ShiftThree());
		
	}
	
	// Update is called once per frame
	void Update () {
		if (normalSpeed == true) {
			tf.position += tf.up * baseSpeed * hundredDivide;
		}
		if (canShift == false){
			if (canMove == true) {
				if (Input.GetKey (KeyCode.W)) {
					tf.position += tf.up * moveSpeed * hundredDivide;
				}
			}
		}
		if (canShift == true) {
			shiftText.gameObject.SetActive (true);
			canMove = false;
			if (Input.GetKeyUp (KeyCode.LeftShift)) {
				canMove = true;
				canShift = false;
				shiftText.gameObject.SetActive (false);
			}
		}
	}
	IEnumerator ReadyPlayerOne () {
		yield return new WaitForSeconds (startTimer);
		canMove = true;
		normalSpeed = true;
	}
	IEnumerator ShiftOne () {
		yield return new WaitForSeconds (shiftOne);
		canShift = true;
	}
	IEnumerator ShiftTwo () {
		yield return new WaitForSeconds (shiftTwo);
		canShift = true;
	}
	IEnumerator ShiftThree () {
		yield return new WaitForSeconds (shiftThree);
		canShift = true;
	}
}
