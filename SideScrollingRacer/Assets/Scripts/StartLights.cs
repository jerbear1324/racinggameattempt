﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartLights : MonoBehaviour {
	public Transform lightOne;
	public Transform lightTwo;
	public Transform lightThree;

	// Use this for initialization
	void Start () {
		StartCoroutine(ReadyRed());
		//Start of Level
		
		
	}
	IEnumerator ReadyRed () {
		//Red Light active
		yield return new WaitForSeconds (1.0f);
		lightOne.gameObject.SetActive (true);
		yield return new WaitForSeconds (1.0f);
		StartCoroutine(SteadyYellow());
		//switch to yellow
	}
	IEnumerator SteadyYellow () {
		//disable red light
		lightOne.gameObject.SetActive (false);
		//activate yellow light
		lightTwo.gameObject.SetActive (true);
		yield return new WaitForSeconds (1.0f);
		StartCoroutine(GoGreen());
		//start of racing
	}
	IEnumerator GoGreen () {
		//disable yellow light
		lightTwo.gameObject.SetActive (false);
		//enable red light
		lightThree.gameObject.SetActive (true);
		yield return new WaitForSeconds (1.0f);
		//destroy starting lights
		Destroy (this.gameObject);
	}

}
