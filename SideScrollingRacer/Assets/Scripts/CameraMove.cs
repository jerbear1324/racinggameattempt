﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {
	public float moveSpeed;
	public float startDelay;
	public float endRace;
	public bool canMove;

	void Start () {
		canMove = false;
		StartCoroutine (ReadyPlayerOne());
	}

	IEnumerator ReadyPlayerOne () {
		yield return new WaitForSeconds (startDelay);
		canMove = true;
		StartCoroutine (EndOfRace ());
	}

	void Update () {
		if (canMove == true) {
			transform.position += transform.up * moveSpeed;
		}
		else
		{
			//do nothing
		}
	}
	IEnumerator EndOfRace () {
		yield return new WaitForSeconds (endRace);
		canMove = false;
	}
}
