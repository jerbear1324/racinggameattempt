﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndofRace : MonoBehaviour {
	public AudioClip endRace;
	private AudioSource source;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D ( Collider2D other ) {
		source.PlayOneShot (endRace);
		Debug.Log ("EndIt");
	}
		
}
